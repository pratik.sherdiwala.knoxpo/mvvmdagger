package com.example.employeeintentusingdagger

import com.example.employeeintentusingdagger.model.Employee

interface TaskItemNavigator {

    fun updateEmployee(id:String)
}