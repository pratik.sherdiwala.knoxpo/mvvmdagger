package com.example.employeeintentusingdagger.ui.main

import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.employeeintentusingdagger.Event
import com.example.employeeintentusingdagger.R
import com.example.employeeintentusingdagger.TaskItemNavigator
import com.example.employeeintentusingdagger.TaskNavigator
import com.example.employeeintentusingdagger.databinding.ActivityMainBinding
import com.example.employeeintentusingdagger.ui.employeedetail.EmployeeDetailActivity
import com.example.employeeintentusingdagger.ui.main.adapter.EmployeeAdapter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

private val TAG = MainActivity::class.java.simpleName

class MainActivity : AppCompatActivity(), TaskNavigator, TaskItemNavigator {


    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory


    private val viewModel by lazy {
        ViewModelProviders
            .of(
                this,
                viewModelFactory
            )
            .get(
                MainViewModel::class.java
            )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        val bindig = DataBindingUtil
            .setContentView<ActivityMainBinding>(
                this,
                R.layout.activity_main
            )

        val activity = this

        with(bindig) {
            lifecycleOwner = activity
            viewModel = activity.viewModel

            itemsRV.layoutManager = LinearLayoutManager(activity)
            itemsRV.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
            itemsRV.adapter = EmployeeAdapter(activity.viewModel)
        }

        with(viewModel) {
            //getEmployee()

            newTaskEvent.observe(activity, Observer<Event<Unit>> { event ->
                event.getContentIfNotHandled()?.let {
                    activity.addNewTask()
                }
            })

            openTaskEvent.observe(
                activity,
                Observer { event ->
                    event.getContentIfNotHandled()?.let {
                        activity.updateEmployee(it)
                    }
                }
            )

            Log.d(TAG, "Now listening to view model (${hashCode()}) to open task")

            fetchEmployees()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_item, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.btnAdd -> {
                viewModel.addNewTask()
            }
        }
        return super.onOptionsItemSelected(item)
    }


    override fun addNewTask() {
        startActivity(
            EmployeeDetailActivity.intentForAddEmployee(this)
        )
    }

    override fun updateEmployee(id: String) {
        startActivity(
            EmployeeDetailActivity.intentForEditEmployee(this, id)
        )
    }

    override fun onResume() {
        super.onResume()
        itemsRV.adapter?.notifyDataSetChanged()
    }
}

/*
D/MainActivity: Now listening to view model (243274671) to open task
D/EmployeeAdapter: Asking for view model (28773755) to open task

D/MainActivity: Now listening to view model (243274671) to open task
Asking for view model (243274671) to open task

 */