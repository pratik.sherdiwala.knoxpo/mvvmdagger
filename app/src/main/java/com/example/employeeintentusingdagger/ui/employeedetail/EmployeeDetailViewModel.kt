package com.example.employeeintentusingdagger.ui.employeedetail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.employeeintentusingdagger.Event
import com.example.employeeintentusingdagger.data.repositery.EmployeeRepository
import com.example.employeeintentusingdagger.model.Employee
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class EmployeeDetailViewModel @Inject constructor(private val employeeRepository: EmployeeRepository) : ViewModel() {

    companion object {
        private val TAG = EmployeeDetailViewModel::class.java.simpleName
    }

    val id = MutableLiveData<String>()
    val name = MutableLiveData<String>()
    val age = MutableLiveData<String>()
    val salary = MutableLiveData<String>()

    val addEmployeeCoomand = MutableLiveData<Employee>()

    val errorAddingEmployee = MutableLiveData<Employee>()


    private val disposables = CompositeDisposable()


    fun updateEmployee() {
//        employeeRepository.updateEmployee(id.toString())
//            .subscribe(
//                {
//                    Log.d(TAG, "onSuccess()")
//                    this.id.value=it.id
//                    name.value = it.name
//                    age.value = it.age
//                    salary.value = it.salary
//                },
//                {
//                    Log.e(TAG, "Failure", it)
//                }
//            )
//            .also {
//                disposables.add(it)
//            }
        val employee = Employee(
            this.id.value.toString(),
            this.name.value.toString(),
            this.age.value.toString(),
            this.salary.value.toString()
        )
        employeeRepository.uodateEmployee(employee)
    }

    fun getEmployee(id: String) {
        employeeRepository.getEmployeeDetail(id)
            .subscribe(
                {
                    this.id.value = it.id
                    name.value = it.name
                    age.value = it.age
                    salary.value = it.salary
                },
                {
                    Log.d(TAG, "Error in fetching employee", it)
                }
            )
            .also {
                disposables.add(it)
            }
    }

    fun createEmployeeDetails() {

        val employee = Employee(
            name = name.value!!,
            age = age.value!!,
            salary = salary.value!!
        )
        employeeRepository.addEmployee(employee)
        addEmployeeCoomand.value = employee
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }
}