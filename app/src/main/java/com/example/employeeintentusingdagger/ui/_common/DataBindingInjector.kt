package com.example.employeeintentusingdagger.ui._common

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.employeeintentusingdagger.model.Employee
import com.example.employeeintentusingdagger.ui.main.adapter.EmployeeAdapter

object DataBindingInjector {

    @JvmStatic
    @BindingAdapter("items")
    fun setEmployee(view:RecyclerView, items:List<Employee>?){
        items?.let {
            (view.adapter as? EmployeeAdapter)?.updateEmployee(it)
        }
    }
}