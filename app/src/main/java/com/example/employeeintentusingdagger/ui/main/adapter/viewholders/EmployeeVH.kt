package com.example.employeeintentusingdagger.ui.main.adapter.viewholders

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.employeeintentusingdagger.R
import com.example.employeeintentusingdagger.TaskItemNavigator
import com.example.employeeintentusingdagger.model.Employee

class EmployeeVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val mNameTV = itemView.findViewById<TextView>(R.id.nameTV)
    private val mIdTV = itemView.findViewById<TextView>(R.id.idTV)
    private val mAgeTV = itemView.findViewById<TextView>(R.id.ageTV)
    private val mSalaryTV = itemView.findViewById<TextView>(R.id.salaryTV)

    fun bindEmployee(employee: Employee, employeeClick: (employee: Employee) -> Unit) {
        mIdTV.text = employee.id
        mNameTV.text = employee.name
        mAgeTV.text = employee.age
        mSalaryTV.text = employee.salary

        itemView.setOnClickListener {
            employeeClick(employee)
        }
    }
}