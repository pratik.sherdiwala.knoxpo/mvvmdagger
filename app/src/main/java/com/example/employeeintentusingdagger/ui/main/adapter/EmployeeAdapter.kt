package com.example.employeeintentusingdagger.ui.main.adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.employeeintentusingdagger.R
import com.example.employeeintentusingdagger.TaskItemNavigator
import com.example.employeeintentusingdagger.model.Employee
import com.example.employeeintentusingdagger.ui.main.MainViewModel
import com.example.employeeintentusingdagger.ui.main.adapter.viewholders.EmployeeVH
import javax.inject.Inject

private val TAG = EmployeeAdapter::class.java.simpleName

class EmployeeAdapter(private val mainViewModel: MainViewModel) : RecyclerView.Adapter<EmployeeVH>() {

    private var items: List<Employee>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmployeeVH {
        return EmployeeVH(
            LayoutInflater.from(parent.context)
                .inflate(
                    R.layout.item_list,
                    parent,
                    false
                )
        )
    }

    override fun getItemCount() = items?.size ?: 0

    override fun onBindViewHolder(holder: EmployeeVH, position: Int) {
        holder.bindEmployee(items!![position]){

            Log.d(TAG, "Asking for view model (${mainViewModel.hashCode()}) to open task")

            mainViewModel.openTask(it.id)
        }
    }

    fun updateEmployee(newEmployee: List<Employee>) {
        items = newEmployee
        notifyDataSetChanged()
    }
}