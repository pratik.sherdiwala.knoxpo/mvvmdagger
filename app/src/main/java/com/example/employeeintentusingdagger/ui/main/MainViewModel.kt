package com.example.employeeintentusingdagger.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.employeeintentusingdagger.Event
import com.example.employeeintentusingdagger.data.repositery.EmployeeRepository
import com.example.employeeintentusingdagger.model.Employee
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class MainViewModel @Inject constructor(private val employeeRepository: EmployeeRepository) : ViewModel() {

    private val disposables = CompositeDisposable()

    val itemList = MutableLiveData<List<Employee>>()

    fun getEmployee() {
        itemList.value = employeeRepository.getEmployees()
    }

    fun fetchEmployees() {
        employeeRepository.getEmployeesFromNetwork()
            .subscribe(
                {
                    itemList.value = it
                },
                {

                }
            )
            .also {
                disposables.add(it)
            }
    }

    override fun onCleared() {
        super.onCleared()
        disposables.dispose()
    }

    private val _newTaskEvent = MutableLiveData<Event<Unit>>()
    val newTaskEvent: LiveData<Event<Unit>>
        get() = _newTaskEvent

    // This LiveData depends on another so we can use a transformation.
    val empty: LiveData<Boolean> = Transformations.map(itemList) {
        it.isEmpty()
    }

    fun
            addNewTask() {
        _newTaskEvent.value = Event(Unit)
    }


    private val _openTaskEvent = MutableLiveData<Event<String>>()
    val openTaskEvent: LiveData<Event<String>>
        get() = _openTaskEvent

    internal fun openTask(taskId: String) {
        _openTaskEvent.value = Event(taskId)
    }
}