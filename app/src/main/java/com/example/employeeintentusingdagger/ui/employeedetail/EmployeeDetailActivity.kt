package com.example.employeeintentusingdagger.ui.employeedetail

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.example.employeeintentusingdagger.R
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.second_activity.*
import javax.inject.Inject

class EmployeeDetailActivity : AppCompatActivity() {

    companion object {
        private val TAG = EmployeeDetailActivity::class.java.simpleName

        private const val ACTION_ADD = 0
        private const val ACTION_EDIT = 1

        private val EXTRA_ACTION = "$TAG.EXTRA_ACTION"
        private val EXTRA_EMPLOYEE_ID = "$TAG.EXTRA_EMPLOYEE_ID"
        private val EXTRA_NAME = "$TAG.EXTRA_NAME"
        private val EXTRA_AGE = "$TAG>EXTRA_AGE"
        private val EXTRA_SALARY = "$TAG.EXTRA_SALARY"

        fun intentForAddEmployee(context: Context): Intent {
            return Intent(context, EmployeeDetailActivity::class.java)
                .apply {
                    putExtra(EXTRA_ACTION, ACTION_ADD)
                }
        }

        fun intentForEditEmployee(context: Context, employeeID: String): Intent {

            return Intent(context, EmployeeDetailActivity::class.java)
                .apply {
                    putExtra(EXTRA_EMPLOYEE_ID, employeeID)
                    putExtra(EXTRA_ACTION, ACTION_EDIT)
                }
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProviders.of(
            this,
            viewModelFactory
        )
            .get(
                EmployeeDetailViewModel::class.java
            )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)


        val binding =
            DataBindingUtil.setContentView<com.example.employeeintentusingdagger.databinding.SecondActivityBinding>(
                this,
                R.layout.second_activity
            )

        val activity = this

        with(binding) {
            lifecycleOwner = activity
            employeeDetailVM = viewModel
        }

        when (intent.getIntExtra(EXTRA_ACTION, -1)) {
            ACTION_ADD -> {
                createBTN.setOnClickListener {
                    addEmployee()
                }
                createBTN.setText("ADD")

            }
            ACTION_EDIT -> {
                val id = intent.getStringExtra(EXTRA_EMPLOYEE_ID)
                viewModel.getEmployee(id)
                updateEmployee()
                createBTN.setText("UPDATE")
                createBTN.setOnClickListener {
                    viewModel.updateEmployee()
                }
            }
        }

        with(viewModel) {
            //updateEmployee()

            addEmployeeCoomand.observe(this@EmployeeDetailActivity,
                Observer {
                    finish()
                })

            errorAddingEmployee.observe(this@EmployeeDetailActivity,
                Observer {
                    setResult(Activity.RESULT_CANCELED)
                    finish()
                })
        }


    }

    fun addEmployee() {
        with(viewModel) {
            createEmployeeDetails()
        }
    }

    fun updateEmployee() {


    }
}