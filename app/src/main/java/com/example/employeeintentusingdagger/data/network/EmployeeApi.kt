package com.example.employeeintentusingdagger.data.network

import com.example.employeeintentusingdagger.model.Employee
import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Path

interface EmployeeApi {

    @GET("employees")
    fun getEmployees(): Single<List<Employee>>

    @PUT("create")
    fun createEmployee(): Single<Employee>

    @PUT("update/{id}")
    fun updateEmployee(@Path("id") id: String?): Single<Employee>
}