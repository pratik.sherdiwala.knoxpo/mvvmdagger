package com.example.employeeintentusingdagger.data.repositery

import com.example.employeeintentusingdagger.data.network.EmployeeApi
import com.example.employeeintentusingdagger.model.Employee
import com.example.employeeintentusingdagger.model.EmployeesLab
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.lang.Exception
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class EmployeeRepository @Inject constructor(
    private val employeeApi: EmployeeApi,
    private val employeesLab: EmployeesLab
) {

    fun getEmployees(): List<Employee> {
        return employeesLab.itemList
    }

    fun getEmployeesFromNetwork(): Single<List<Employee>> {
        /*return employeeApi.getEmployees()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())*/

        return Single.just(employeesLab.itemList)
    }

//    fun createEmployee(): Single<Employee> {
//        return employeeApi.createEmployee()
//            .subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//    }

    fun updateEmployee(id: String): Single<Employee> {
        return employeeApi.updateEmployee(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun getEmployeeDetail(id: String): Single<Employee> {
        val employee = employeesLab.itemList.find {
            it.id == id
        }

        return if (employee == null) {
            Single.error(Exception("Employee not found"))
        } else {
            Single.just(employee)
        }
    }


    fun addEmployee(employee: Employee) {
        employeesLab.addEmployee(employee)
    }

    fun uodateEmployee(employee: Employee) {
        // employeesLab.itemList.set(employee.id.toInt() - 1, employee)
        employeesLab.editEmployee(employee)
    }
}