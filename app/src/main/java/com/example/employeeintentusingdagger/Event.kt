package com.example.employeeintentusingdagger

open class Event<out T>(private val content: T) {

    var hasBeenhandled = false;

    fun getContentIfNotHandled(): T? {
        return if (hasBeenhandled) {
            null
        } else {
            hasBeenhandled = true
            content
        }
    }
    fun peekContent(): T = content
}