package com.example.employeeintentusingdagger.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.employeeintentusingdagger.ui.main.MainViewModel
import com.example.employeeintentusingdagger.viewmodel.AppViewModelFactory
import com.example.employeeintentusingdagger.ui.employeedetail.EmployeeDetailViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    abstract fun provideViewModelFactory(appViewModelFactory: AppViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(EmployeeDetailViewModel::class)
    abstract fun bindSecondViewModel(employeeDetailVM: EmployeeDetailViewModel):ViewModel
}
