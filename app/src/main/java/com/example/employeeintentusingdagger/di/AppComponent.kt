package com.example.employeeintentusingdagger.di

import com.example.employeeintentusingdagger.App
import com.example.employeeintentusingdagger.di.module.AppModule
import com.example.employeeintentusingdagger.di.module.ViewModelModule
import com.example.employeeintentusingdagger.di.module.ui.MainActivityModule
import com.example.employeeintentusingdagger.di.module.NetworkModule
import com.example.employeeintentusingdagger.di.module.ui.EmployeeEditActivityModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ViewModelModule::class,
        MainActivityModule::class,
        NetworkModule::class,
        EmployeeEditActivityModule::class,
        AppModule::class
    ]
)

interface AppComponent {

    fun inject(app: App)
}
