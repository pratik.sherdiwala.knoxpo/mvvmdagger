package com.example.employeeintentusingdagger.di.module

import com.example.employeeintentusingdagger.data.network.EmployeeApi
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@Module
class NetworkModule {

    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("http://dummy.restapiexample.com/api/v1/")
            .build()
    }

    @Provides
    fun provideEmployeeApi(retrofit: Retrofit): EmployeeApi{
        return retrofit.create(EmployeeApi::class.java)
    }
}