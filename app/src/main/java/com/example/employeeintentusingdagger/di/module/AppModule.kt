package com.example.employeeintentusingdagger.di.module

import com.example.employeeintentusingdagger.model.EmployeesLab
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule {

    @Provides
    fun provideEmployeeLab(): EmployeesLab {
        return EmployeesLab()
    }


}