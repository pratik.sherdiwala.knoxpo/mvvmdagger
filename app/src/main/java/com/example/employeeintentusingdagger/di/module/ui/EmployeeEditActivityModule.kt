package com.example.employeeintentusingdagger.di.module.ui

import com.example.employeeintentusingdagger.ui.employeedetail.EmployeeDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class EmployeeEditActivityModule {

    @ContributesAndroidInjector
    abstract fun ontributeEmployeeEditActivity(): EmployeeDetailActivity
}