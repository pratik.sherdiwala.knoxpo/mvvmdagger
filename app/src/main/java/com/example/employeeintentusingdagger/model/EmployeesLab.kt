package com.example.employeeintentusingdagger.model

import java.util.*

class EmployeesLab {

    var itemList = mutableListOf<Employee>()

    init {
        for (i in 1..20) {
            itemList.add(
                Employee(
                    i.toString(),
                    "Employee ${Date().time}",
                    (i * 30).toString(),
                    (i * itemList.size + 10).toString()
                )
            )
        }
    }

    fun addEmployee(employee: Employee) {

        employee.id = (itemList.size + 1).toString()

        itemList.add(employee)
    }

    fun editEmployee(employee: Employee){
        itemList.set(itemList.size-1,employee)
    }
}