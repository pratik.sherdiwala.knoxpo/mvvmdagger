package com.example.employeeintentusingdagger.model

import com.google.gson.annotations.SerializedName

data class Employee(

    var id: String = "",

    @SerializedName("employee_name")
    val name: String,

    @SerializedName("employee_salary")
    val salary: String,

    @SerializedName("employee_age")
    val age: String

)